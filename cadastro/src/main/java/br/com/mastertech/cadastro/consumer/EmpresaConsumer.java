package br.com.mastertech.cadastro.consumer;

import br.com.mastertech.cadastro.service.CadastroService;
import br.com.mastertech.empresa.producer.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;


@Component
public class EmpresaConsumer {
    @Autowired
    private CadastroService cadastroService;

    @KafkaListener(topics = "spec3-saulo-aroni-2", groupId = "saulo-1")
    public void receber(@Payload Empresa empresa)  {
       System.out.println("Cnpj"+ empresa.getCnpj());

       // cadastroService.pesquisaCapital("33611500000119");
       cadastroService.pesquisaCapital(empresa.getCnpj());
    }
}