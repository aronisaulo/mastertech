package br.com.mastertech.cadastro.consumer.client;

import br.com.mastertech.cadastro.producer.CadastroDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
/*https://www.receitaws.com.br/v1/cnpj/33611500000119*/

@FeignClient(name= "receita" , url = "https://www.receitaws.com.br" )
public interface CadastroClient {

    @GetMapping("/v1/cnpj/{cnpj}")
    CadastroDTO getByCnpj(@PathVariable String cnpj);


}
