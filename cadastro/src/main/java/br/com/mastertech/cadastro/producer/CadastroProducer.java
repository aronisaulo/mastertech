package br.com.mastertech.cadastro.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
@Service
public class CadastroProducer {
    @Autowired
    private KafkaTemplate<String , CadastroDTO> producer;

    public void enviarAoKafka(CadastroDTO cadastroDTO){

        System.out.println("Kafka" + cadastroDTO.getDados());

        producer.send("spec3-saulo-aroni-3", cadastroDTO);

    }
}
