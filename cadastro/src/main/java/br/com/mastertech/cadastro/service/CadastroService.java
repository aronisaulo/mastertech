package br.com.mastertech.cadastro.service;

import br.com.mastertech.cadastro.consumer.client.CadastroClient;
import br.com.mastertech.cadastro.producer.CadastroDTO;
import br.com.mastertech.cadastro.producer.CadastroProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CadastroService {

    @Autowired
    private CadastroClient cadastroClient;

    @Autowired
    private CadastroProducer cadastroProducer;

    public void pesquisaCapital(String cnpj){
        CadastroDTO cadastroDTO =  cadastroClient.getByCnpj(cnpj);
        if (!cadastroDTO.getDados().isEmpty())
            cadastroProducer.enviarAoKafka(cadastroDTO);
    }

}
