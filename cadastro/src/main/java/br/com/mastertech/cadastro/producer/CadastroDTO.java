package br.com.mastertech.cadastro.producer;

public class CadastroDTO {
    private  String dados;

    public String getDados() {
        return dados;
    }

    public void setDados(String dados) {
        this.dados = dados;
    }
}
