package br.com.mastertech.salvacadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalvacadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalvacadastroApplication.class, args);
	}

}
