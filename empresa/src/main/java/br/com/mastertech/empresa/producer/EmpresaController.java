package br.com.mastertech.empresa.producer;

import br.com.mastertech.empresa.models.EmpresaDTO;
import br.com.mastertech.empresa.producer.Empresa;
import br.com.mastertech.empresa.producer.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    EmpresaProducer empresaProducer;

    @PostMapping
    public void cadastraEmpresa(@RequestBody EmpresaDTO empresaDTO){

        System.out.println("Empresa >"+empresaDTO.getCnpj());
        Empresa empresa = new Empresa();
        empresa.setCnpj(empresaDTO.getCnpj());
        empresaProducer.enviarAoKafka(empresa);
    }
}
