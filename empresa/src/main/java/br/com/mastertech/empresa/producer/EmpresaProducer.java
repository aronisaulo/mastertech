package br.com.mastertech.empresa.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {
    @Autowired
    private KafkaTemplate<String , Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        System.out.println(empresa.getCnpj());
        producer.send("spec3-saulo-aroni-2",0, "", empresa  );
    }
}
